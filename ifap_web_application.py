from read_and_process import *

try:
    filepath = input("Please give Population csv path")
    task_input = input("Please enter 1 for Task1   or enter 2 for Task2 :")
    InputData_.city = input("Please enter city")
    InputData_.year = input("Please enter year. Or press enter to get data for latest year")
    if int(task_input) == 1:
        check_reliability = False
    elif int(task_input) == 2:
        check_reliability = True
    readCSV(filepath)
    print(get_data_from_population(check_reliability))
except:
    raise Exception

