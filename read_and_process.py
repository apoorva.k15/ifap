import pandas as pd


class InputData:
    def __init__(self):
        self.population_data = pd.DataFrame
        self.city = ""
        self.year = ""
        self.include_provisional = str(True)


InputData_ = InputData()


def readCSV(filepath):
    # read the CSV
    try:
        InputData_.population_data = pd.read_csv(filepath)
    except:
        raise Exception


def get_data_from_population(check_reliability):
    output_data = ""
    try:
        city_population = InputData_.population_data.loc[InputData_.population_data['City'] == InputData_.city]
        if InputData_.year != "":
            city_population_for_year = city_population.loc[city_population['Year'] == InputData_.year]
        else:
            city_population_for_year = city_population.loc[city_population['Year'] == city_population['Year'].max()]

        if check_reliability:
            if InputData_.include_provisional == "True":
                include_prov_str = "Provisional figure"
            else:
                include_prov_str = "Final figure, complete"
            city_population_for_year_with_prov_flag = city_population_for_year.loc[city_population_for_year['Reliability'] == include_prov_str]
            output_data = {"Population": city_population_for_year_with_prov_flag['Value'].iloc[0]}
        else:
            output_data = {"Population": city_population_for_year['Value'].iloc[0]}
        return output_data
    except:
        raise Exception
